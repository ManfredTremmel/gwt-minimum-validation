package de.knightsoftnet.minvalidation;

import jakarta.validation.Validator;

import de.knightsoftnet.validators.client.AbstractGwtValidatorFactory;
import de.knightsoftnet.validators.client.GwtValidation;
import de.knightsoftnet.validators.client.impl.AbstractGwtValidator;

public class MyValidatorFactory extends AbstractGwtValidatorFactory {

  @GwtValidation(Submission.class)
  public interface GwtValidator extends Validator {
  }

  @Override
  public AbstractGwtValidator createValidator() {
    return new GwtValidatorImpl();
  }
}

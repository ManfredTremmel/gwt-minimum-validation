package de.knightsoftnet.minvalidation;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	void greetServer(Submission input, AsyncCallback<GreetingResponse> callback);
}

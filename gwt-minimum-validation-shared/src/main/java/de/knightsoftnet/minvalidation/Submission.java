package de.knightsoftnet.minvalidation;

import java.io.Serializable;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public class Submission implements Serializable {
  private static final long serialVersionUID = -4363319846243587870L;

  @NotEmpty
  @Size(min = 3)
  private String name;

  public Submission() {
	super();
  }

  public Submission(String name) {
	super();
	this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}

package de.knightsoftnet.minvalidation;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.Set;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
		GreetingService {

	public GreetingResponse greetServer(Submission input) throws IllegalArgumentException {
		// Verify that the input is valid.
	    final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
	    final Set<ConstraintViolation<Submission>> violations = validator.validate(input);
	    if (!violations.isEmpty()) {
	      // If the input is not valid, throw an IllegalArgumentException back to
	      // the client.
	      throw new IllegalArgumentException(
	          violations.iterator().next().getMessage());
	    }

		GreetingResponse response = new GreetingResponse();

		response.setServerInfo(getServletContext().getServerInfo());
		response.setUserAgent(getThreadLocalRequest().getHeader("User-Agent"));

		response.setGreeting("Hello, " + input.getName() + "!");

		return response;
	}
}
